# Sanasto index

Tämän projektin JSON -tiedosto `index.json` sisältää hakemiston eli indeksin eri sanastoihin.

Sanastot sisältävät termejä eri tietotekniikan alan osa-alueista. Näitä on tarkoitus käyttää opetuksessa mutta niitä voi hyödyntää myös itseopiskellen.

Tällä hetkellä sanastoja ovat:

1. Perussanasto tietokoneista.
2. Tietorakenteiden ja algoritmien sanasto.

Molemmat sanastot ovat keskeneräisiä ja työn alla. Sanastot löytyvät osoitteesta https://gitlab.com/sanasto.

Sanastot ovat koneluettavia. Niitä voi tarkastella millä tahansa sovelluksella joka voi esittää JSON -muotoista tietoa ihmiselle sopivassa muodossa.

Työn alla on myös:

* Java Swing:llä toteutettu sovellus joka toimii kaikissa työpöytäkäyttöjärjestelmissä joissa JVM, sekä
* Swift:llä toteutettu sovellus joka toimii Applen alustoilla.

## Kontribuutio

Jos haluat lisätä uuden sanaston tai osallistua sanastojen sisältöjen kehittämiseen, tutustu `index.json`:n rakenteeseen ja yllä mainittuihin esimerkkisanastoihin. Jos löydät sanastoista korjattavaa tai täydennettävää, ota yhteyttä ylläpitäjiin tai tee Issue GitLab:n projektiin.

